# 简单宝可梦对战

基于EasyX实现的简易宝可梦对战

- 简单的用户界面

- 人物对话

- 简单的剧情演绎

- 大地图人物移动

- 回合制精灵对战

![](https://gitlab.com/aiit-ai-lab/ailab-newpeoplesee/easyx-tutorial/easyBoKEMen/-/raw/main/%E5%8F%A3%E8%A2%8B%E5%A6%96%E6%80%AA.gif?ref_type=heads&inline=false)
